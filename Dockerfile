FROM ubuntu:latest
MAINTAINER Michael Dodwell <michael@dodwell.us>

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update --quiet \
    && apt-get install -qq --yes --no-install-recommends nfs-kernel-server runit inotify-tools git module-init-tools

RUN mkdir -p /exports

RUN mkdir -p /etc/sv/nfs
ADD files/nfs.init /etc/sv/nfs/run
ADD files/nfs.stop /etc/sv/nfs/finish

RUN echo "nfs 2049/tcp" >> /etc/services

ADD files/nfs_setup.sh /usr/local/bin/nfs_setup

VOLUME /exports

EXPOSE 111/udp 2049/tcp

ENTRYPOINT ["/usr/local/bin/nfs_setup"]
