#!/bin/bash

set -e

mounts="${@}"

for mnt in "${mounts[@]}"; do
  src=$(echo $mnt | cut -d: -f1)
  git=$(echo $mnt | cut -d: -f2-)
  if [ "x${git}" != "x" -a "x${git}" != "x${src}" ]; then
    GIT_SSL_NO_VERIFY=true git clone $git tmp && mv tmp/.git $src && rm -rf tmp && cd $src && git reset --hard
  else
    mkdir -p $src
  fi
  echo "$src *(rw,sync,no_subtree_check,fsid=0,no_root_squash)" >> /etc/exports
done

exec runsvdir /etc/sv
